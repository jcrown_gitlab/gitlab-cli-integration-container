# Gitlab CLI Integration Container

Thin wrapper container around https://gitlab.com/gitlab-org/cli for use in pipelines.

Quick example

```yaml
stages:          
  - playground

#Need a private token named private_token with access to read and create issue etc.  
Echo:
  image: registry.gitlab.com/jcrown_gitlab/gitlab-cli-integration-container:latest
  stage: playground
  script:
  - |
    echo "Authenticating using a Project Access Token"
    glab auth login --stdin < $private_token
    
    echo "Listing current issues"
    glab issue list

    echo "Creating an issue"
    ISSUE=`sed -ne "s/.*\(http[^\"]*\).*/\1/p" | glab issue create -t "Testing Issue created" --label bug --description "Everything is not fine" -y`
    
    echo "Listing current issues"
    glab issue list

    #With a project access token we are unable to delete issues etc.  Need a personal access token instead.
    echo "Closing the previous issue."
    glab issue close $ISSUE

    echo "Listing current Merge Requests"
    glab mr list

    echo "List closed issues"
    glab issue list -A
```
