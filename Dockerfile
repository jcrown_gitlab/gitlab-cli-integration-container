FROM golang:1.19-alpine as builder

RUN apk add --no-cache bash \
                       curl \
                       docker-cli \
                       git \
                       mercurial \
                       make \
                       build-base \
            && git clone https://gitlab.com/gitlab-org/cli.git /go/src/glab \
            && cd /go/src/glab \
            && tag=$(git describe --tags `git rev-list --tags --max-count=1`) \
            && git checkout $tag -b latest \
            && make \
            && cp bin/glab /bin/glab

FROM alpine

RUN apk add --no-cache git \
                        jq \
                        coreutils

COPY --from=builder /bin/glab /bin/glab
